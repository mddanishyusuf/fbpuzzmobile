// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('fbpuzzmobile', ['ionic', 'starter.controllers','videopage.controllers','custom.code','com.2fdevs.videogular','com.2fdevs.videogular.plugins.controls','com.2fdevs.videogular.plugins.overlayplay','com.2fdevs.videogular.plugins.poster','com.2fdevs.videogular.plugins.buffering'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

    }
        if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
$stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
    })
    .state('app.videolists', {
        url: '/videolists',
        views: {
            'videoContents': {
                templateUrl: 'views/videolist.html',
                controller: 'VideolistsCtrl'
            }
        }    
    })    
    .state('app.pagelist', {
        url: '/pagelist',
            views: {
            'videoContents': {
                templateUrl: 'views/pagelist.html',
                controller: 'PageListController'
            }
        }
    })
    .state('video_page', {
        url: '/play',
        templateUrl: 'views/video_page.html',
        controller: 'SingleVideoCtrl'
    })
    .state('video_page.play', {
        url: '/v/:page_id/:post_id',
        views: {
            'videoPlayer': {
                templateUrl: 'views/videoplayer.html',
                controller: 'videoPlayerCtrl'
            },
            'realtedVideos': {
                templateUrl: 'views/relatedvideos.html',
                controller: 'realtedVideosCtrl'
            }
        }    
    })
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/videolists');
});
