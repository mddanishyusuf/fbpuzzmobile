angular.module('videopage.controllers', [])

.controller('VideolistsCtrl', function($scope, $http) {
    $scope.loadmore = true;
    $scope.homelist = true;
    var l = 2
    var result = []

    $http({
        method: "GET",
        url: "https://api.fbpuzz.com/data.js"
        }).success(function(data) {
            angular.forEach(data, function(value) {
            result.push(value)
        })
    }).error(function() {
        console.log('Error');
    }).finally(function() {
        $scope.loadmore = false;
        $scope.homelist = false;
    })
    $scope.result = result


    $scope.loadMore = function() {
    console.log(l)
        var url = "https://api.fbpuzz.com/api/getposts/" + l;
        $http({
            method: "GET",
            url: url
            }).success(function(data) {            
            angular.forEach(data, function(value) {
            result.push(value)
            $scope.$broadcast('scroll.infiniteScrollComplete');
            })
        }).error(function() {
            console.log("Error");
        }).finally(function() {
            $scope.loadmore = false;
        })
        $scope.result = result 
        l = l + 1;
    };
})

.controller('SingleVideoCtrl', function($scope, $stateParams) {
    $scope.return_time = function(tm){
        return moment.unix(tm).fromNow()
    }
    $scope.return_length = function(tm){
        d = Number(tm);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + ":" : "";
        var mDisplay = m > 0 ? m + ":" : "00:";
        var sDisplay = s > 0 ? (s>9 ? s : "0"+s) : "00";
        return hDisplay + mDisplay + sDisplay; 
    }
})

.controller('videoPlayerCtrl', function($scope, $http, $stateParams, $sce){

    var url = 'https://api.fbpuzz.com/api/video/'+$stateParams.page_id+'/'+$stateParams.post_id
    $scope.page_name = $stateParams.page_id
    $scope.post_id = $stateParams.post_id

    $scope.videogularDiv = false
    $http.get(url).then(function(response){
        $scope.vid = response.data
        var video_data = response.data
        $scope.video_link = response.data.source
        var category_name = video_data.content_category
        $scope.description = video_data.description
        $scope.short_description = video_data.description ? video_data.description.substr(0, 50) : video_data.description;
        $scope.vid_title = video_data.title ? video_data.title : ($scope.page_name ? $scope.page_name + ' Video' : $scope.short_description)
        // document.title = $scope.vid_title
        // $analytics.eventTrack('Click', {  category: category_name, label: $scope.vid_title });

        var poster = response.data.format[1].picture ? response.data.format[1].picture : ( response.data.format[0].picture ? response.data.format[0].picture : ( response.data.format[0].picture ? response.data.format[0].picture : response.data.picture))

        $scope.config = {
            preload: "none",
            sources: [
                {src: $sce.trustAsResourceUrl(response.data.source), type: "video/mp4"}
            ],
            theme: "css/videogular.min.css",
            plugins: {
            poster: poster,
            controls: {
                autoHide: true,
                autoHideTime: 3000
            },         
            // analytics: {
            //     category: "Videogular",
            //     label: "Main",
            //     events: {
            //         ready: true,
            //         play: true,
            //         pause: true,
            //         stop: true,
            //         complete: true,
            //         progress: 10
            //     }
            // }
            }
        };
    }, 
    function(response){
        // failure call back
        console.log('error')
    }).finally(function(){
        $scope.videogularDiv = true
    });


})

.controller('realtedVideosCtrl', function($scope, $http, $stateParams, $sce, $rootScope) {
    // $scope.loadmore = false;
    $scope.loadmore = true;
    url4 = "https://api.fbpuzz.com/api/getpageInfo/" + $stateParams.page_id
    $http.get(url4).then(function(response){
        $scope.page_info = response.data
    },function(response){
        // failure call back
        console.log('error')
    })

    var l = 2;
    var url2 = "https://api.fbpuzz.com/api/get/" + $stateParams.page_id + '?count=4'
    var result = []
    $http.get(url2).then(function(response){
        angular.forEach(response.data, function(value) {
            result.push(value)
        })
        var url3 = "https://api.fbpuzz.com/api/getposts/" + response.data[0].content_category
        $scope.vid_content_category = response.data[0].content_category
        $http.get(url3).then(function(response){
            angular.forEach(response.data, function(value) {
            result.push(value)
            })
        },function(response){
            // failure call back
        })
    },function(response){
        // failure call back
    }).finally(function() {
        // $scope.show_more_load = true;
        $scope.loadmore = false;
    })

    $scope.related_data = result
    $scope.loadMore = function(cat) {
        var url3 = "https://api.fbpuzz.com/api/getposts/" + cat + "/" + l 
        var url = url3
        $http({
            method: "GET",
            url: url,
            cache: false
        }).success(function(data) {          
            angular.forEach(data, function(value) {
            result.push(value)
            })
            $scope.$broadcast('scroll.infiniteScrollComplete');
        }).error(function() {
            console.log("Error");
        }).finally(function() {
            $scope.loadmore = false;
        })
        $scope.related_data = result 
        l = l + 1;
    };

})